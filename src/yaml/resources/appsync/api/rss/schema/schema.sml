type Api {
	feed(url: String): Feed
}

type Feed {
	items: [Item]
}

type Item {
	title: String
	guid: String
	category: [String]
	link: String
	description: String
	pubDate: String
	location: String
}

type Query {
	rss: Api
}

schema {
	query: Query
}

