# graphql-schema-rss

## Installation

    git clone https://bitbucket.org/mswinson-lib/graphql-schema-rss  

## Prequisites

    docker
    docker-compose

## Usage

setup  

    docker-compose build sandbox  

    export S3_BUCKET_REPO=<mybucket>
    export S3_BUCKET_PREFIX=<mybucket_prefix>
    export AWS_DEFAULT_REGION=<aws_region>

build  

    docker-compose run sandbox  

    > make build


deploy

    docker-compose run sandbox

    > make deploy
  


